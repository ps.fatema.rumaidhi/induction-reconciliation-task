<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link
 href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 rel="stylesheet"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
 crossorigin="anonymous">
</head>
<body>

 <div class="container col-lg-6">
  <div class="card">
   <div class="card-body">
    <form method="post" class="form-group" action="sourceUploadServlet"  enctype="multipart/form-data">
     <div class="form-group">
      <label for="source name">Sourc Name: </label>
      <input type="text" class="form-control" name="sourcename" size="50" />
     </div>
     <div class="form-group">
      <label for="File Type ">File Type : </label>
      <input type="text" class="form-control" name="fileTypes" size="50" />
     </div>

     <div class="form-group">
      <label for="File to uploade">File to uploade:</label>
      <input type="file" name="uploadedFile" size="50" />
     </div>
     <input type="submit" value="next" class="btn btn-success">
    </form>
   </div>
  </div>
 </div>
</body>
</html>

