package com.progressoft.jip9.reconciliation.Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login";
        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = request.getRequestURI().equals(loginURI);

        IfLoggedInOrLoginRequest(chain, request, response, loginURI, loggedIn, loginRequest);
    }

    private void IfLoggedInOrLoginRequest(FilterChain chain, HttpServletRequest request, HttpServletResponse response, String loginURI, boolean loggedIn, boolean loginRequest) throws ServletException, IOException {
        if (loggedIn || loginRequest) {
            try {
                chain.doFilter(request, response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect(loginURI);
        }
    }

}