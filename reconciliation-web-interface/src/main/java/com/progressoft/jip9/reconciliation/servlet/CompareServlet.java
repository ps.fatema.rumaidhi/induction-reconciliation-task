package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.DataStoring.FilesData;
import com.ps.jip9.TransComparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Paths;


public class CompareServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FilesData fileData2 = (FilesData) req.getSession().getAttribute("filesData");
        String sourceName=fileData2.getSourceName();
        String uploadDir = "./target/tmp/uploadFiles/";//call compare method which return 3 lists
        TransComparator transComparator =new TransComparator();
        transComparator.compair(Paths.get(uploadDir + "source"), Paths.get(uploadDir + "target"),"","");
        //save these lists in sessions and create object for it

        //display them in next page

    }
}
