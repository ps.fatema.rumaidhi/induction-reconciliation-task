package com.progressoft.jip9.reconciliation.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class DownloadServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out =resp.getWriter();
        String filename = req.getParameter("match")+".csv";//.............
        String filenameJSON = req.getParameter("match")+".json";
        String filepath = "./target/tmp/uploadFiles/";
        resp.setContentType("APPLICATION/OCTET-STREAM");
        resp.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");
        FileInputStream fileInputStream=new FileInputStream(filepath + filename);
        downloadFile(out, fileInputStream);
    }

    private void downloadFile(PrintWriter out, FileInputStream fileInputStream) throws IOException {
        int i;
        while ((i=fileInputStream.read()) != -1) {
            out.write(i);
        }
        fileInputStream.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}