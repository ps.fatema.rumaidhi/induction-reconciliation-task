package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.DataStoring.FilesData;
import com.progressoft.jip9.reconciliation.uploadFile.FileUploadDao;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@MultipartConfig(maxFileSize = 16177215)//up to 16MB
public class TargetUploadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        uploadFileToServelt(req);//to the server @ /target/tmp/uploadedFiles
        FilesData fileData2 = (FilesData) req.getSession().getAttribute("filesData");
        fileData2.setTargetType(req.getParameter("targetType"));
        fileData2.setTargetName(req.getParameter("targetName"));
        redirectToNextPage(req, resp, fileData2);
    }

    private void uploadFileToServelt(HttpServletRequest req) throws IOException, ServletException {
        Part uploadedFile = req.getPart("uploadedTargetFile");
        uploadedFile.write("target");
    }

    private void redirectToNextPage(HttpServletRequest req, HttpServletResponse resp, FilesData fileData2) throws ServletException, IOException {
        req.getSession().setAttribute("filesData", fileData2);
        req.getRequestDispatcher("/compareFiles.jsp").forward(req, resp);
    }
}

    /*
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FilesData filesData= (FilesData) request.getAttribute("fileDataObject");
        String targetName = request.getParameter("targetname");
        String fileTypeTarget = request.getParameter("fileTypesTarget");
        filesData.setTargetName(targetName);
        filesData.setTargetType(fileTypeTarget);


        PrintWriter out = response.getWriter();
            HttpSession session=request.getSession(false);
            out.print("<h4>Welcome"+ session.getAttribute("user"));
            if(session != null){
                out.print("Welcome"+ session.getAttribute("user"));

            }

            //uploaded-file: in serlvet only
            ServletFileUpload servletFileUpload= new ServletFileUpload(new DiskFileItemFactory());
        try {
            List<FileItem> filesList = servletFileUpload.parseRequest(request);
            for (FileItem item : filesList) {
                String filePath = "D:/json/induction-reconciliation-task/reconciliation-web-interface/target/tmp/"
                        + item.getName();
                filesData.setTargetPath(filePath);
                item.write(new File(filePath));

            }
            request.setAttribute("FileData2",filesData);
            request.getRequestDispatcher("/compareFiles.jsp").forward(request, response);


             } catch (FileUploadException e) {
                e.printStackTrace();//...................................
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

     */

