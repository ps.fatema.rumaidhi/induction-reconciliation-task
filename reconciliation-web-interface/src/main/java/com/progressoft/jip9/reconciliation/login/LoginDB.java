package com.progressoft.jip9.reconciliation.login;

import java.sql.*;

public class LoginDB {

    public boolean validate(LoginHelper loginHelper) throws ClassNotFoundException, SQLExceptionErrors {
        boolean status = false;
        Class.forName("com.mysql.jdbc.Driver");
        try (Connection connection = getConnection(); PreparedStatement preparedStatement = connection
                     .prepareStatement(sqlQuery())) {
            status = prepareAndExecuteQuery(loginHelper, preparedStatement);

        } catch (SQLException e) {
            throw new SQLExceptionErrors(e);
        }
        return status;
    }

    private boolean prepareAndExecuteQuery(LoginHelper loginHelper, PreparedStatement preparedStatement) throws SQLException {
        boolean status;
        preparedStatement.setString(1, loginHelper.getUsername());
        preparedStatement.setString(2, loginHelper.getPassword());
        System.out.println(preparedStatement);
        ResultSet rs = preparedStatement.executeQuery();
        status = rs.next();
        return status;
    }

    private Connection getConnection() throws SQLException {
        return DriverManager
                .getConnection("jdbc:mysql://localhost:3306/reconciliation?useSSL=false", "root", "KILOkilo1234!@#$");
    }

    private String sqlQuery() {
        return "select * from login where username = ? and password = ? ";
    }

}

