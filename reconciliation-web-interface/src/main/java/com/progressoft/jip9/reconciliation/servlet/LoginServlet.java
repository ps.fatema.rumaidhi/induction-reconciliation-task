package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.login.LoginDB;
import com.progressoft.jip9.reconciliation.login.LoginHelper;
import com.progressoft.jip9.reconciliation.login.SQLExceptionErrors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

//@WebServlet("/login")              /****************************************************/

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private LoginDB loginDB;

    public void init() {
        loginDB = new LoginDB();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        LoginHelper loginHelper = new LoginHelper();
        setUserAndPassword(username, password, loginHelper);
        checkLogin(request, response, username, loginHelper);
    }
    private void checkPass(String plainPassword, String hashedPassword) {
        if (BCrypt.checkpw(plainPassword, hashedPassword))
            System.out.println("The password matches.");
        else
            System.out.println("The password does not match.");
    }

    private String hashPassword(String plainTextPassword){
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }
    private void checkLogin(HttpServletRequest request, HttpServletResponse response, String username, LoginHelper loginHelper) throws ServletException, IOException {
        try {
            if (loginDB.validate(loginHelper)) {
                validLogin(request, response, username);
            } else {
                InvalidLogin(request, response, username);
            }
        } catch (ClassNotFoundException | SQLExceptionErrors e) {
           // throw new  SQLExceptionErrors(e);
            e.printStackTrace();
        }
    }

    private void InvalidLogin(HttpServletRequest request, HttpServletResponse response, String username) throws IOException, ServletException {
        HttpSession session = request.getSession();
        session.setAttribute("user", username);
        PrintWriter out = response.getWriter();
        out.print("Sorry, username or password error!");
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    private void validLogin(HttpServletRequest request, HttpServletResponse response, String username) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("user", username);
        session.setMaxInactiveInterval(600);//set session period to 10 mintuse
        session.invalidate();
        request.getRequestDispatcher("/sourceUpload.jsp").forward(request, response);
    }

    private void setUserAndPassword(String username, String password, LoginHelper loginHelper) {
        loginHelper.setUsername(username);
        loginHelper.setPassword(password);
    }
}

