package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.DataStoring.FilesData;
import com.progressoft.jip9.reconciliation.uploadFile.FileUploadDao;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.IOException;


@MultipartConfig(maxFileSize = 16177215)//up to 16MB
public class SourceUploadServlet  extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        uploadFileToServelt(req);
      FilesData filesData = new FilesData();
      filesData.setSourceType(req.getParameter("fileType"));
      filesData.setSourceName(req.getParameter("sourceName"));
        redirectToNextPage(req, resp, filesData);

    }

    private void uploadFileToServelt(HttpServletRequest req) throws IOException, ServletException {
        Part uploadedFile = req.getPart("uploadedFile");
        uploadedFile.write("source");
    }

    private void redirectToNextPage(HttpServletRequest req, HttpServletResponse resp, FilesData filesData) throws ServletException, IOException {
        req.getSession().setAttribute("filesData" , filesData);
        req.getRequestDispatcher("/targetUpload.jsp").forward(req, resp);
    }
}

//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//
//        FilesData filesData=new FilesData();
//        PrintWriter out = response.getWriter();
//        HttpSession session=request.getSession(false);
////        out.print("<h4>Welcome"+ session.getAttribute("user"));
//        if(session != null){
//            out.print("Welcome"+ session.getAttribute("user"));
//        }
//        //uploaded-file: in serlvet only
//        ServletFileUpload servletFileUpload= new ServletFileUpload(new DiskFileItemFactory());
//        try {
//            List<FileItem> filesList=servletFileUpload.parseRequest(request);
//            for (FileItem item: filesList) {
//                String filePath= "D:/json/induction-reconciliation-task/reconciliation-web-interface/target/tmp/"
//                        + item.getName();
//                filesData.setSourcePath(filePath);
//                item.write(new File(filePath));
//            }
//        } catch (FileUploadException e) {
//            e.printStackTrace();//...................................
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        //uploaded-file : DB
//        String sourceName = request.getParameter("sourcename");
//        String fileType = request.getParameter("fileTypes");
//        filesData.setSourceName(sourceName);
//        filesData.setSourceType(fileType);
//
//        InputStream inputStream = null; // input stream of the upload file
//        String message = null;
//        // obtains the upload file part in this multipart request
//        Part filePart = request.getPart("uploadedFile");
//        if (filePart != null) {
//            // prints out some information for debugging
//            System.out.println(filePart.getName());
//            System.out.println(filePart.getSize());
//            System.out.println(filePart.getContentType());
//
//            // obtains input stream of the upload file
//            inputStream = filePart.getInputStream();
//        }
//        // sends the statement to the database server
//        int row = fileUploadDao.uploadFile(sourceName, fileType, inputStream);
//        if (row > 0) {
//            System.out.println("File uploaded and saved into database");//...................
//            message = "File uploaded and saved into database";
//        }
//        // sets the message in request scope
//        request.setAttribute("Message", message);//........................................
//
//        // forwards to the target page
//
//        request.getSession().setAttribute("fileData1",filesData);
//        FilesData fileData1 = (FilesData) request.getSession().getAttribute("fileData1");
//        request.getRequestDispatcher("/targetUpload.jsp").forward(request, response);
//        //getServletContext().getRequestDispatcher("/targetUpload.jsp").forward(request, response);
//    }
//}
