package com.progressoft.jip9.reconciliation.initializer;

import com.progressoft.jip9.reconciliation.login.LoginDB;
import com.progressoft.jip9.reconciliation.login.LoginHelper;
import com.progressoft.jip9.reconciliation.servlet.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;


public class reconciliationInitializer implements ServletContainerInitializer {


    @Override
    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
        registerLoginServlet(ctx);
        registerLogoutServlet(ctx);
        registerSourceUploadServlet(ctx);
        registerTargetUploadServlet(ctx);
        registerCompareServlet(ctx);
        registerDownloadServlet(ctx);


    }

    private void registerLoginServlet(ServletContext ctx) {
        LoginServlet servlet = new LoginServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("LoginServlet", servlet);
        servletRegistration.addMapping("/login");
    }



    private void registerLogoutServlet(ServletContext ctx) {
        LogoutServlet servlet = new LogoutServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("LogoutServlet", servlet);
        servletRegistration.addMapping("/logout");
    }


    private void registerSourceUploadServlet(ServletContext ctx) {

        SourceUploadServlet servlet = new SourceUploadServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("SourceUploadServlet", servlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploadedFiles", 1048576, 1048576, 262144));
        servletRegistration.addMapping("/sourceUpload");

    }

    private void registerTargetUploadServlet(ServletContext ctx) {
        TargetUploadServlet servlet = new TargetUploadServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("TargetUploadServlet", servlet);
        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploadedFiles", 1048576, 1048576, 262144));
        servletRegistration.addMapping("/targetUpload");

    }

    private void registerCompareServlet(ServletContext ctx) {
        CompareServlet servlet = new CompareServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("CompareServlet", servlet);
        servletRegistration.addMapping("/compareFiles");

    }

    private void registerDownloadServlet(ServletContext ctx) {
        DownloadServlet servlet = new DownloadServlet();
        ServletRegistration.Dynamic servletRegistration = ctx.addServlet("DownloadServlet", servlet);
        servletRegistration.addMapping("/download");
    }
}