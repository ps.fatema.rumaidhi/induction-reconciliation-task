package com.progressoft.jip9.reconciliation.uploadFile;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class FileUploadDao {
    private static final String sql = "INSERT INTO users (source_name, file_type, file_uploaded) values (?, ?, ?)";

    public int uploadFile(String sourcName, String fileType, InputStream fileUploaded) {
        int row = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e1) {

            e1.printStackTrace();
        }

        row = connectToDB(sourcName, fileType, fileUploaded, row);
        return row;
    }

    private int connectToDB(String sourcName, String fileType, InputStream fileUploaded, int row) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection
                     .prepareStatement(sql)) {
            fillPreparedStatementWithInfo(sourcName, fileType, fileUploaded, preparedStatement);
            row = preparedStatement.executeUpdate();

        } catch ( SQLException e) {
            printSQLException(e);
        }
        return row;
    }

    private void fillPreparedStatementWithInfo(String sourcName, String fileType, InputStream fileUploaded, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setString(1, sourcName);
        preparedStatement.setString(2, fileType);
        if (fileUploaded != null) {
            preparedStatement.setBlob(3, fileUploaded);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager
                .getConnection("jdbc:mysql://localhost:3306/reconciliation?useSSL=false", "root", "KILOkilo1234!@#$");
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
}