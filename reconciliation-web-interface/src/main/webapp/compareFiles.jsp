<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<%@page import="com.progressoft.jip9.reconciliation.*"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Source Upload</title>
</head>
<body>
 <div align="center">
    <form action="<%=request.getContextPath()%>/compareFiles"  method="post" enctype="multipart/form-data">
     <table style="with: 300%">
       <tr>
       <td>
             <table border="1">
                 <caption>Source</caption>
                   <tr>
                    <td >Name : </td><td >${filesData.getSourceName()}  </td>
                  </tr>
                  <tr>
                    <td >Type : </td><td > ${filesData.getSourceType()} </td>
                  </tr>
            </table>
       </td>
        </tr>
       <br />

    <tr>
       <td>
             <table border="1">
                 <caption>Target</caption>
                   <tr>

                    <td >Name : </td><td >${filesData.getTargetName()}</td>
                  </tr>
                  <tr>
                    <td >Type : </td><td >${filesData.getTargetType()} </td>
                  </tr>
            </table>
       </td>
        </tr>
      <br /><br />
      <tr>
       <td>Result Files Format</td>
      </tr>
       <br /><br />
        <tr>
       <td>
          <select name="resultFormat" id="result" style="width: 300px;">

               <option>csv</option>
               <option>json</option>

           </select>
        </td>
      </tr>
     <tr>


      <br /><br />
      <br /><br />
     </table>
       <input class="button" type="button"
              onclick="window.location.replace('http://localhost:8080/reconciliation/sourceUpload')"
              value="Cancel"/>
      <input type=button value="Back" onCLick="history.back()">
        <t />
       <input type="Submit" value="Compare" />

  </form>

  <form action="<%=request.getContextPath()%>/logout"  enctype="multipart/form-data">
     <input type="submit" value="logout" />
  </form>
  </div>
 </body>
 </html>




