package com.ps.jip9;

import java.io.IOException;

// TODO naming convention
public class readCSVException extends IOException {

    public readCSVException(String message) {
        super(message);
    }

    public readCSVException(String message, Throwable cause) {
        super(message, cause);
    }
}
