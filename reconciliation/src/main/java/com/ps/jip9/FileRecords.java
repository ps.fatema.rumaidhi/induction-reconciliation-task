package com.ps.jip9;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public interface FileRecords {
    String getTransUniqueID();

    String getTransDescription();

    BigDecimal getAmount();

    String getPurpose();

    String getCurrecny();

    LocalDate getDate();

    TransType getTransType();

}
