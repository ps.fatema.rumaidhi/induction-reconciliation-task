package com.ps.jip9;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CSVData extends TransData {
    private String transUniqueId;
    private String transDescription;
    private BigDecimal amount;
    private String purpose;
    private String currecny;
    private LocalDate date;
    private TransType transType;

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getTransUniqueId() {
        return transUniqueId;
    }

    public void setTransUniqueId(String transUniqueId) {
        this.transUniqueId = transUniqueId;
    }

    public String getTransDescription() {
        return transDescription;
    }

    public void setTransDescription(String transDescription) {
        this.transDescription = transDescription;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrecny() {
        return currecny;
    }

    public void setCurrecny(String currecny) {
        this.currecny = currecny;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public TransType getTransType() {
        return transType;
    }

    public void setTransType(char transType) {
        this.transType = TransType.getTransType(transType);
    }

}
