package com.ps.jip9;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransComparator {
    private final String CSV_FORMAT = "csv";
    private final String JSON_FORMAT = "json";

    Map<String, TransData> sourceDataList = new HashMap<>();
    Map<String, TransData> targetDataList = new HashMap<>();

    FilesReader filesReader;

    public TransComparator() {

    }

    public TransComparator(FilesReader filesReader) {
        this.filesReader = filesReader;
    }


    public void compair(Path csvFileLocation, Path jsonFileLocation, String sourceFileFormat, String targetFileFormat) {
        if (sourceFileFormat.equalsIgnoreCase(CSV_FORMAT)) {
            fullCsvList(csvFileLocation);
        } else if (sourceFileFormat.equalsIgnoreCase(JSON_FORMAT)) {

            fullJsonList(jsonFileLocation);
        }

        if (targetFileFormat.equalsIgnoreCase(JSON_FORMAT)) {
            fullJsonList(jsonFileLocation);

        } else if (targetFileFormat.equalsIgnoreCase(CSV_FORMAT)) {

            fullCsvList(csvFileLocation);
        }


        //Match

        for (String index : sourceDataList.keySet()) {
            if (sourceDataList.containsKey(index) && targetDataList.containsKey(index)) {
                // TODO toooo long condition
                if (sourceDataList.get(index).getAmount().equals(targetDataList.get(index).getAmount()) &&
                        sourceDataList.get(index).getCurrecny().equals(targetDataList.get(index).getCurrecny()) &&
                        sourceDataList.get(index).getValueDate().equals(targetDataList.get(index).getValueDate()) &&
                        sourceDataList.get(index).getCurrecny().equals(targetDataList.get(index).getCurrecny())) {
                    // TODO printing out is not the final result, we need to print it to a file
                    System.out.print("Match");
                    System.out.print("  " + targetDataList.get(index).getTransUniqueId());
                    System.out.print("  " + targetDataList.get(index).getAmount());
                    System.out.print("  " + targetDataList.get(index).getCurrecny());
                    System.out.println("  " + targetDataList.get(index).getValueDate());
                }
                System.out.println("misMatch");
                System.out.print("  " + targetDataList.get(index).getTransUniqueId());
                System.out.print("  " + targetDataList.get(index).getAmount());
                System.out.print("  " + targetDataList.get(index).getCurrecny());
                System.out.println("  " + targetDataList.get(index).getValueDate());

            }
        }


    }

    private Map<String, TransData> fullCsvList(Path csvFileLocation) {
        List<CSVData> csvDataList = null;
        try {
            csvDataList = filesReader.readCSV(csvFileLocation);
        } catch (readCSVException e) {
            //TODO swallowing exception
            e.printStackTrace();  //................................
        }
        return getSourceDataList(csvDataList);
    }

    private Map<String, TransData> getSourceDataList(List<CSVData> csvDataList) {
        for (int i = 0; i < csvDataList.size(); i++) {
            TransData transData = new TransData();
            transData.setTransUniqueId(csvDataList.get(i).getTransUniqueId());
            transData.setAmount(csvDataList.get(i).getAmount());
            transData.setCurrecny(csvDataList.get(i).getCurrecny());
            transData.setValueDate(csvDataList.get(i).getDate());
            sourceDataList.put(csvDataList.get(i).getTransUniqueId(), transData);
        }
        return sourceDataList;
    }

    private Map<String, TransData> fullJsonList(Path jsonFileLocation) {
        List<JSONData> jsonDataList = filesReader.readJSON(jsonFileLocation);

        return getTargetDataList(jsonDataList);
    }

    private Map<String, TransData> getTargetDataList(List<JSONData> jsonDataList) {
        for (int i = 0; i < jsonDataList.size(); i++) {
            TransData transData = new TransData();
            transData.setTransUniqueId(jsonDataList.get(i).getReference());
            transData.setAmount(jsonDataList.get(i).getAmount());
            transData.setCurrecny(jsonDataList.get(i).getCurrencyCode());
            transData.setValueDate(jsonDataList.get(i).getDate());
            targetDataList.put(jsonDataList.get(i).getReference(), transData);
        }


        return targetDataList;
    }


}


