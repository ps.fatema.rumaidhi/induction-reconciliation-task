package com.ps.jip9;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class FilesReader {

    private Path csvFileLocation;
    private Path  jsonFileLocation;
    List<CSVData> csvDataList=new ArrayList<>();
    List<JSONData> jsonDataList=new ArrayList<>();

    public FilesReader(Path fileLocation) {
        this.csvFileLocation = fileLocation;
    }

    public FilesReader(Path csvFileLocation, Path jsonFile) {
        this.csvFileLocation = csvFileLocation;
        this.jsonFileLocation = jsonFile;
    }

    public FilesReader() {

    }

    public List<CSVData> readCSV(Path csvFileLocation) throws readCSVException {
        if(csvFileLocation==null){
            throw new NullPointerException("null path");
        }
        if(!csvFileLocation.toFile().exists()){
           throw new readCSVException("Not Exist");
        }

        try (BufferedReader reader = Files.newBufferedReader(csvFileLocation)) {
            String line;
            String firstLine= reader.readLine();//.......................
            while ((line = reader.readLine()) != null) {
                String[] csvRecords = line.split(",");
                CSVData csvData= new CSVData();
                csvData.setTransUniqueId(csvRecords[0]);
                csvData.setTransDescription(csvRecords[1]);
                csvData.setAmount(new BigDecimal(csvRecords[2]));
                csvData.setCurrecny(csvRecords[3]);
                csvData.setPurpose(csvRecords[4]);
                LocalDate date= LocalDate.parse(csvRecords[5], DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                csvData.setDate(date);
                csvData.setTransType(csvRecords[6].charAt(0));
                csvDataList.add(csvData);

            }

        } catch (IOException e) {
           throw new readCSVException("error while read CSV File",e);
        }

        return csvDataList;
    }

    public List<JSONData> readJSON(Path jsonFileLocation) {
        try{
        String reader = Files.readString(jsonFileLocation);
             JSONArray jsonArray = new JSONArray(reader);
            for(int i=0;i<jsonArray.length();i++){
                JSONData jsonData= new JSONData();
                JSONObject e = jsonArray.getJSONObject(i);
                jsonData.setReference(e.getString("reference"));
                 String date= e.getString("date");
                LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                jsonData.setDate(localDate);
                String amount= e.getString("amount");
                BigDecimal amount1= new BigDecimal(amount);
                jsonData.setAmount(amount1);
                jsonData.setCurrencyCode(e.getString("currencyCode"));
                jsonData.setPurpose(e.getString("purpose"));
                jsonDataList.add(jsonData);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonDataList;
    }
}
