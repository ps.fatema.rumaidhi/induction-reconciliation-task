package com.ps.jip9;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TransData {

    private String fileType;
    private BigDecimal amount;
    private String currecny;
    private String transUniqueId;
    private LocalDate valueDate;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getTransUniqueId() {
        return transUniqueId;
    }

    public void setTransUniqueId(String transUniqueId) {
        this.transUniqueId = transUniqueId;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    public void setValueDate(LocalDate valueDate) {
        this.valueDate = valueDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrecny() {
        return currecny;
    }

    public void setCurrecny(String currecny) {
        this.currecny = currecny;
    }


}
