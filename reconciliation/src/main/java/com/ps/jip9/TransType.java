package com.ps.jip9;

public enum TransType {
    D,C;

    public static TransType getTransType(char transType) {

        switch (transType){
            case 'D': return  D;
            case 'C': return C;
            default: return null;
        }
    }
}
