package com.ps.jip9;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {


    public static void main(String[] args) throws IOException {
         Path path1= Paths.get(".","sample-files","input-files");
       Path csvfilePath= path1.resolve("bank-transactions.csv");
       Path jsonFilePath= path1.resolve("online-banking-transactions.json");
        FilesReader filesReader = new FilesReader();
     //  filesReader.readCSV(csvfilePath);
       //filesReader.readJSON(jsonFilePath);

        TransComparator transComparator=new TransComparator(filesReader);
        transComparator.compair(csvfilePath,jsonFilePath,"csv","json");

    }
}