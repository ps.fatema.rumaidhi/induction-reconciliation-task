import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConsoleUserTest {
    @Test
    public void givenNullSourceFileLocation_whenPromptUser_thenFail(){
       ConsoleUser consoleUser=new ConsoleUser();
        NullPointerException nullException = Assertions.assertThrows(NullPointerException.class, () -> consoleUser.promptUser());
        Assertions.assertEquals("null source file location",nullException.getMessage());

    }

    @Test
    public void givenEmptySourceFileLocation_whenPromptUser_thenFail(){

    }
    @Test
    public void givenNotExistSourceFileLocation_whenPromptUser_thenFail(){

    }
    @Test
    public void givenExistSourceFileLocation_whenPromptUser_thenSuccess(){

    }
    @Test
    public void givenNullsourceFileFormat_whenPromptUser_thenFail(){

    }
    @Test
    public void givenWrongSourceFileFormat_whenPromptUser_thenFail(){

    }

    @Test
    public void givenCorrectSourceFileFormat_whenPromptUser_thenFail(){

    }


}


