package com.ps.jip;

import com.ps.jip9.TransComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TransComparatorTest {

    @BeforeEach
    public void setUp(){
        Path path1= Paths.get(".","sample-files","input-files");
        // TODO useless assignment
        Path csvfilePath= path1.resolve("bank-transactions.csv");
        Path jsonFilePath= path1.resolve("online-banking-transactions.json");
    }
    // TODO where is success scenario
    @Test
    public void givenNullSourceFileFormat_whenCompair_thenFail(){
        Path path1= Paths.get(".","sample-files","input-files");
        Path csvfilePath= path1.resolve("bank-transactions.csv");
        Path jsonFilePath= path1.resolve("online-banking-transactions.json");
        TransComparator transComparator=new TransComparator();
        String sourceFileFormat =null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,()->
                transComparator.compair(csvfilePath,jsonFilePath,null,"json"));
        Assertions.assertEquals("null path", exception.getMessage());

    }
}
