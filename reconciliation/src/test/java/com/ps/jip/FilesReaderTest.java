package com.ps.jip;

import com.ps.jip9.CSVData;
import com.ps.jip9.FilesReader;
import com.ps.jip9.TransType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

public class FilesReaderTest {


    private FilesReader reader;

    @BeforeEach
    public void setup() {
        reader = new FilesReader();
    }

    @Test
    public void canCreate() {
        new FilesReader();
    }

    @Test
    public void givenNullFilePath_whenReadCSV_thenFail(){
        Path csvFileLocation =null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,()->reader.readCSV(csvFileLocation));
        Assertions.assertEquals("null path", exception.getMessage());

    }

    @Test
    public void givenNotExistsPath_whenReadCSV_thenFail() {
        Path csvFileLocation = Paths.get(".", "dir" + new Random().nextInt());
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> reader.readCSV(csvFileLocation));
        Assertions.assertEquals("path does not exists", thrown.getMessage());
    }

    @Test
    public void givenInvalidDirectoryPath_whenReadCSV_thenFail() throws IOException {
        Path csvFileLocation = Files.createTempDirectory("bis-dir");
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () ->reader.readCSV(csvFileLocation));
        Assertions.assertEquals("path is not a file", thrown.getMessage());
    }

    @Test
    public void givenValidPath_whenReadCSV_thenSuccess() throws IOException {
        Path path= Paths.get("..","sample-files","input-files","bank-transactions.csv");
        List<CSVData>records=reader.readCSV(path);

        Assertions.assertNotNull(records);
        Assertions.assertEquals(records.size(), 6, "expected 6 records");

        CSVData csvRecord = records.get(0);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222201", csvRecord.getTransUniqueId());
        Assertions.assertEquals("online transfer", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(140).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("USD", csvRecord.getCurrecny());
        Assertions.assertEquals("donation", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-01-20"),csvRecord.getDate());
        Assertions.assertEquals(TransType.D, csvRecord.getTransType());

         csvRecord = records.get(1);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222202", csvRecord.getTransUniqueId());
        Assertions.assertEquals("atm withdrwal", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(20.0000).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("JOD", csvRecord.getCurrecny());
        Assertions.assertEquals("", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-01-22"),csvRecord.getDate());
        Assertions.assertEquals(TransType.D, csvRecord.getTransType());

         csvRecord = records.get(2);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222203", csvRecord.getTransUniqueId());
        Assertions.assertEquals("counter withdrawal", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(5000).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("JOD", csvRecord.getCurrecny());
        Assertions.assertEquals("", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-01-25"),csvRecord.getDate());
        Assertions.assertEquals(TransType.D, csvRecord.getTransType());

         csvRecord = records.get(3);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222204", csvRecord.getTransUniqueId());
        Assertions.assertEquals("salary", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(1200.000).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("JOD", csvRecord.getCurrecny());
        Assertions.assertEquals("donation", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-01-31"),csvRecord.getDate());
        Assertions.assertEquals(TransType.C, csvRecord.getTransType());

         csvRecord = records.get(4);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222205", csvRecord.getTransUniqueId());
        Assertions.assertEquals("atm withdrwal", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(60.0).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("JOD", csvRecord.getCurrecny());
        Assertions.assertEquals("", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-02-02"),csvRecord.getDate());
        Assertions.assertEquals(TransType.D, csvRecord.getTransType());

        csvRecord = records.get(5);
        Assertions.assertNotNull(csvRecord);
        Assertions.assertEquals("TR-47884222206", csvRecord.getTransUniqueId());
        Assertions.assertEquals("atm withdrwal", csvRecord.getTransDescription());
        Assertions.assertEquals(BigDecimal.valueOf(500.0).setScale(2, RoundingMode.HALF_UP), csvRecord.getAmount().setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals("USD", csvRecord.getCurrecny());
        Assertions.assertEquals("", csvRecord.getPurpose());
        Assertions.assertEquals(LocalDate.parse("2020-02-10"),csvRecord.getDate());
        Assertions.assertEquals(TransType.D, csvRecord.getTransType());

    }
}
